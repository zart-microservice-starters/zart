# zART

![](https://img.shields.io/gitlab/pipeline-status/zart-microservice-starters/zart?branch=development&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/zart-microservice-starters/zart/development?logo=jest&style=for-the-badge)

<br/>

![](https://gitlab.com/-/snippets/2378316/raw/main/zart.svg)
