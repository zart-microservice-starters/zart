import { createReactQueryHooks } from "@trpc/react";
// eslint-disable-next-line import/no-extraneous-dependencies
import { AppRouter } from "@zart-starters/gateway/src/app";

export default createReactQueryHooks<AppRouter>();
