import React from "react";
import {
  Center,
  useColorMode,
  Tooltip,
  IconButton,
  SunIcon,
  MoonIcon,
  VStack,
  Button,
  Input,
  HStack,
  Skeleton,
  Heading,
  Box,
} from "native-base";
import trpc from "../utils/trpc";

function ColorModeSwitch(): JSX.Element {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Tooltip
      label={colorMode === "dark" ? "Enable light mode" : "Enable dark mode"}
      placement="bottom right"
      openDelay={300}
      closeOnClick={false}
    >
      <IconButton
        position="absolute"
        top={12}
        right={8}
        onPress={toggleColorMode}
        icon={colorMode === "dark" ? <SunIcon /> : <MoonIcon />}
        accessibilityLabel="Color Mode Switch"
      />
    </Tooltip>
  );
}
type Path = "hellopy" | "hellots" | "hellors";
interface CallFormProps {
  path: Path;
}

function CallForm(p: CallFormProps): JSX.Element {
  const [val, setVal] = React.useState("");
  const [name, setName] = React.useState("");
  const hello = trpc.useQuery([p.path, name]);

  const submitName = (): void => {
    setName(val);
  };

  if (hello.status === "error") {
    return (
      <Box
        style={{
          width: 400,
          height: 62,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Heading>-- {p.path} unavailable --</Heading>;
      </Box>
    );
  }
  if (hello.status === "loading") {
    return (
      <VStack style={{ width: 400 }}>
        <Skeleton h={62} />
      </VStack>
    );
  }

  return (
    <VStack style={{ marginBottom: 20 }}>
      <Heading size="md">{hello.data}</Heading>
      <HStack space={3}>
        <Input
          placeholder="insert text above"
          value={val}
          onChangeText={setVal}
        />
        <Button
          onPress={submitName}
          style={{ width: 200 }}
          colorScheme="coolGray"
        >
          {`Call ${p.path}`}
        </Button>
      </HStack>
    </VStack>
  );
}

export default function App(): JSX.Element {
  return (
    <Center
      flex={1}
      _dark={{ bg: "blueGray.900" }}
      _light={{ bg: "blueGray.50" }}
    >
      <VStack space="md">
        {["hellopy", "hellots", "hellors"].map((p) => (
          <CallForm key={p} path={p as Path} />
        ))}
      </VStack>
      <ColorModeSwitch />
    </Center>
  );
}
