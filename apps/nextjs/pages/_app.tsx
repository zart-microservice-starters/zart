import React from "react";
import "../styles/globals.css";
import type { AppProps } from "next/app";
import { NativeBaseProvider } from "native-base";
import { withTRPC } from "@trpc/next";

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <NativeBaseProvider>
      <Component {...pageProps} />
    </NativeBaseProvider>
  );
}

export default withTRPC({
  config() {
    return {
      url: "http://localhost:3001/trpc",
    };
  },
  ssr: true,
})(MyApp);
