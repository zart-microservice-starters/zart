import * as trpc from "@trpc/server";
import * as trpcExpress from "@trpc/server/adapters/express";
import express from "express";
import cors from "cors";
import helloPY from "./services/helloPY";
import helloTS from "./services/helloTS";
import helloRS from "./services/helloRS";

export const appRouter = trpc
  .router()
  .query("hellopy", {
    input: (val: unknown) => val as string,
    resolve: ({ input }) => helloPY(input),
  })
  .query("hellots", {
    input: (val: unknown) => val as string,
    resolve: ({ input }) => helloTS(input),
  })
  .query("hellors", {
    input: (val: unknown) => val as string,
    resolve: ({ input }) => helloRS(input),
  });

export type AppRouter = typeof appRouter;

const app = express();

app.use(cors());

const createContext = ({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  req,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  res,
}: trpcExpress.CreateExpressContextOptions): unknown => ({}); // no context

app.use(
  "/trpc",
  trpcExpress.createExpressMiddleware({
    router: appRouter,
    createContext,
  })
);

export default app;
