import * as Sentry from "@sentry/node";
import app from "./app";
import { loadConfig, setupLogger } from "./utils";

async function main(): Promise<void> {
  const config = await loadConfig();
  const logger = setupLogger(config.logLevel);
  logger.info(`Running with: Config ${JSON.stringify(config)} `);
  Sentry.init({
    dsn: config.sentryUrl,
  });
  app.listen(config.port, config.host);
}

main().then();
