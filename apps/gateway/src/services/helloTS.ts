/* eslint-disable @typescript-eslint/no-explicit-any */
import * as grpc from "@grpc/grpc-js";
import { promisify } from "util";
import rpc from "../proto/hellots_grpc_pb";
import messages from "../proto/hellots_pb";
import { loadConfig } from "../utils";

export default async function helloTS(input: string): Promise<string> {
  const config = await loadConfig();
  const client = new rpc.GreeterClient(
    config.hellotsAddress,
    grpc.credentials.createInsecure()
  );
  const request = new messages.HelloRequest().setName(input);
  const response = (await promisify(client.sayHello.bind(client))(
    request
  )) as messages.HelloReply;
  return response.getMessage();
}
