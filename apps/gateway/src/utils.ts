import winston from "winston";
import { z } from "zod";
import { Address4 as IpV4 } from "ip-address";
import { promises } from "fs";
import { match } from "ts-pattern";
import yaml from "yaml";

const Config = z.object({
  logLevel: z
    .union([
      z.literal("ERROR"),
      z.literal("WARN"),
      z.literal("INFO"),
      z.literal("DEBUG"),
    ])
    .default("DEBUG"),
  port: z.number().min(80).default(3000),
  host: z.string().transform((val) => new IpV4(val).address),
  sentryUrl: z
    .string()
    .url()
    .nullish()
    .transform((val) => val || ""),
  e2e: z.boolean().default(false),
  // todo: when traefik plugin is ready, validate this correctly
  hellotsAddress: z.string(),
  hellopyAddress: z.string(),
  hellorsAddress: z.string(),
});

export type ConfigType = z.infer<typeof Config>;
interface ConfigMap {
  local: ConfigType;
  development: ConfigType;
  beta: ConfigType;
  production: ConfigType;
}

export async function loadConfig(): Promise<ConfigType> {
  // eslint-disable-next-line security/detect-non-literal-fs-filename
  const file = await promises.readFile(
    `${__dirname.slice(0, -3)}/cfg.yml`,
    "utf8"
  );
  const configMap = yaml.parse(file) as ConfigMap;
  const environment = process.env.ENV || "local";
  return match(environment)
    .with("local", () => Config.parse(configMap.local))
    .with("development", () => Config.parse(configMap.development))
    .with("beta", () => Config.parse(configMap.beta))
    .otherwise(() => Config.parse(configMap.production));
}

export function setupLogger(level: string): winston.Logger {
  return winston.createLogger({
    level: level.toLowerCase(),
    format: winston.format.combine(
      winston.format((info) => ({
        ...info,
        level: info.level.toUpperCase(),
      }))(),
      winston.format.colorize(),
      winston.format.printf((info) => `${info.level}: ${info.message}`)
    ),
    transports: [new winston.transports.Console()],
  });
}
