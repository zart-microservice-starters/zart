// GENERATED CODE -- DO NOT EDIT!

"use strict";
var grpc = require("@grpc/grpc-js");
var hellors_pb = require("./hellors_pb.js");

function serialize_hellors_HelloReply(arg) {
  if (!(arg instanceof hellors_pb.HelloReply)) {
    throw new Error("Expected argument of type hellors.HelloReply");
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hellors_HelloReply(buffer_arg) {
  return hellors_pb.HelloReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_hellors_HelloRequest(arg) {
  if (!(arg instanceof hellors_pb.HelloRequest)) {
    throw new Error("Expected argument of type hellors.HelloRequest");
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hellors_HelloRequest(buffer_arg) {
  return hellors_pb.HelloRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

var GreeterService = (exports.GreeterService = {
  sayHello: {
    path: "/hellors.Greeter/SayHello",
    requestStream: false,
    responseStream: false,
    requestType: hellors_pb.HelloRequest,
    responseType: hellors_pb.HelloReply,
    requestSerialize: serialize_hellors_HelloRequest,
    requestDeserialize: deserialize_hellors_HelloRequest,
    responseSerialize: serialize_hellors_HelloReply,
    responseDeserialize: deserialize_hellors_HelloReply,
  },
});

exports.GreeterClient = grpc.makeGenericClientConstructor(GreeterService);
