// GENERATED CODE -- DO NOT EDIT!

"use strict";
var grpc = require("@grpc/grpc-js");
var hellopy_pb = require("./hellopy_pb.js");

function serialize_hellopy_HelloReply(arg) {
  if (!(arg instanceof hellopy_pb.HelloReply)) {
    throw new Error("Expected argument of type hellopy.HelloReply");
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hellopy_HelloReply(buffer_arg) {
  return hellopy_pb.HelloReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_hellopy_HelloRequest(arg) {
  if (!(arg instanceof hellopy_pb.HelloRequest)) {
    throw new Error("Expected argument of type hellopy.HelloRequest");
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hellopy_HelloRequest(buffer_arg) {
  return hellopy_pb.HelloRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

var GreeterService = (exports.GreeterService = {
  sayHello: {
    path: "/hellopy.Greeter/SayHello",
    requestStream: false,
    responseStream: false,
    requestType: hellopy_pb.HelloRequest,
    responseType: hellopy_pb.HelloReply,
    requestSerialize: serialize_hellopy_HelloRequest,
    requestDeserialize: deserialize_hellopy_HelloRequest,
    responseSerialize: serialize_hellopy_HelloReply,
    responseDeserialize: deserialize_hellopy_HelloReply,
  },
});

exports.GreeterClient = grpc.makeGenericClientConstructor(GreeterService);
