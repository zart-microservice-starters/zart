// package: hellors
// file: hellors.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as hellors_pb from "./hellors_pb";

interface IGreeterService
  extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  sayHello: IGreeterService_ISayHello;
}

interface IGreeterService_ISayHello
  extends grpc.MethodDefinition<
    hellors_pb.HelloRequest,
    hellors_pb.HelloReply
  > {
  path: "/hellors.Greeter/SayHello";
  requestStream: false;
  responseStream: false;
  requestSerialize: grpc.serialize<hellors_pb.HelloRequest>;
  requestDeserialize: grpc.deserialize<hellors_pb.HelloRequest>;
  responseSerialize: grpc.serialize<hellors_pb.HelloReply>;
  responseDeserialize: grpc.deserialize<hellors_pb.HelloReply>;
}

export const GreeterService: IGreeterService;

export interface IGreeterServer extends grpc.UntypedServiceImplementation {
  sayHello: grpc.handleUnaryCall<
    hellors_pb.HelloRequest,
    hellors_pb.HelloReply
  >;
}

export interface IGreeterClient {
  sayHello(
    request: hellors_pb.HelloRequest,
    callback: (
      error: grpc.ServiceError | null,
      response: hellors_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  sayHello(
    request: hellors_pb.HelloRequest,
    metadata: grpc.Metadata,
    callback: (
      error: grpc.ServiceError | null,
      response: hellors_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  sayHello(
    request: hellors_pb.HelloRequest,
    metadata: grpc.Metadata,
    options: Partial<grpc.CallOptions>,
    callback: (
      error: grpc.ServiceError | null,
      response: hellors_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
}

export class GreeterClient extends grpc.Client implements IGreeterClient {
  constructor(
    address: string,
    credentials: grpc.ChannelCredentials,
    options?: Partial<grpc.ClientOptions>
  );
  public sayHello(
    request: hellors_pb.HelloRequest,
    callback: (
      error: grpc.ServiceError | null,
      response: hellors_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  public sayHello(
    request: hellors_pb.HelloRequest,
    metadata: grpc.Metadata,
    callback: (
      error: grpc.ServiceError | null,
      response: hellors_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  public sayHello(
    request: hellors_pb.HelloRequest,
    metadata: grpc.Metadata,
    options: Partial<grpc.CallOptions>,
    callback: (
      error: grpc.ServiceError | null,
      response: hellors_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
}
