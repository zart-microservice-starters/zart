// package: hellopy
// file: hellopy.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as hellopy_pb from "./hellopy_pb";

interface IGreeterService
  extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  sayHello: IGreeterService_ISayHello;
}

interface IGreeterService_ISayHello
  extends grpc.MethodDefinition<
    hellopy_pb.HelloRequest,
    hellopy_pb.HelloReply
  > {
  path: "/hellopy.Greeter/SayHello";
  requestStream: false;
  responseStream: false;
  requestSerialize: grpc.serialize<hellopy_pb.HelloRequest>;
  requestDeserialize: grpc.deserialize<hellopy_pb.HelloRequest>;
  responseSerialize: grpc.serialize<hellopy_pb.HelloReply>;
  responseDeserialize: grpc.deserialize<hellopy_pb.HelloReply>;
}

export const GreeterService: IGreeterService;

export interface IGreeterServer extends grpc.UntypedServiceImplementation {
  sayHello: grpc.handleUnaryCall<
    hellopy_pb.HelloRequest,
    hellopy_pb.HelloReply
  >;
}

export interface IGreeterClient {
  sayHello(
    request: hellopy_pb.HelloRequest,
    callback: (
      error: grpc.ServiceError | null,
      response: hellopy_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  sayHello(
    request: hellopy_pb.HelloRequest,
    metadata: grpc.Metadata,
    callback: (
      error: grpc.ServiceError | null,
      response: hellopy_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  sayHello(
    request: hellopy_pb.HelloRequest,
    metadata: grpc.Metadata,
    options: Partial<grpc.CallOptions>,
    callback: (
      error: grpc.ServiceError | null,
      response: hellopy_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
}

export class GreeterClient extends grpc.Client implements IGreeterClient {
  constructor(
    address: string,
    credentials: grpc.ChannelCredentials,
    options?: Partial<grpc.ClientOptions>
  );
  public sayHello(
    request: hellopy_pb.HelloRequest,
    callback: (
      error: grpc.ServiceError | null,
      response: hellopy_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  public sayHello(
    request: hellopy_pb.HelloRequest,
    metadata: grpc.Metadata,
    callback: (
      error: grpc.ServiceError | null,
      response: hellopy_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
  public sayHello(
    request: hellopy_pb.HelloRequest,
    metadata: grpc.Metadata,
    options: Partial<grpc.CallOptions>,
    callback: (
      error: grpc.ServiceError | null,
      response: hellopy_pb.HelloReply
    ) => void
  ): grpc.ClientUnaryCall;
}
