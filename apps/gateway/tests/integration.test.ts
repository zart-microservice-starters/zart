import assert from "assert";

import supertest, { Test, SuperTest } from "supertest";
import {
  loadPackageDefinition,
  sendUnaryData,
  Server,
  ServerUnaryCall,
  ServerCredentials,
} from "@grpc/grpc-js";
import * as protoLoader from "@grpc/proto-loader";
import { GreeterHandlers as GreeterHandlersTs } from "./fixtures/hellots/Greeter";
import { GreeterHandlers as GreeterHandlersPy } from "./fixtures/hellopy/Greeter";
import { GreeterHandlers as GreeterHandlersRs } from "./fixtures/hellors/Greeter";
import { ProtoGrpcType as ProtoGrpcTypeTs } from "./fixtures/hellots";
import { ProtoGrpcType as ProtoGrpcTypePy } from "./fixtures/hellopy";
import { ProtoGrpcType as ProtoGrpcTypeRs } from "./fixtures/hellors";
import { HelloRequest as HelloRequestTs } from "./fixtures/hellots/HelloRequest";
import { HelloRequest as HelloRequestPy } from "./fixtures/hellopy/HelloRequest";
import { HelloRequest as HelloRequestRs } from "./fixtures/hellors/HelloRequest";
import { HelloReply as HelloReplyTs } from "./fixtures/hellots/HelloReply";
import { HelloReply as HelloReplyPy } from "./fixtures/hellopy/HelloReply";
import { HelloReply as HelloReplyRs } from "./fixtures/hellors/HelloReply";
import { loadConfig } from "../src/utils";
import app from "../src/app";

type GreeterHandlers =
  | GreeterHandlersTs
  | GreeterHandlersPy
  | GreeterHandlersRs;
type ProtoGrpcType = ProtoGrpcTypeTs | ProtoGrpcTypePy | ProtoGrpcTypeRs;
type HelloRequest = HelloRequestTs | HelloRequestPy | HelloRequestRs;
type HelloReply = HelloReplyTs | HelloReplyPy | HelloReplyRs;
type PkgName = "hellopy" | "hellots" | "hellors";

function serverSetup(
  returnValue: string,
  pkgName: string
): [GreeterHandlers, ProtoGrpcType] {
  const packageDefinition = protoLoader.loadSync(
    `${__dirname.slice(0, -5)}/${pkgName}.proto`
  );
  const proto = loadPackageDefinition(
    packageDefinition
  ) as unknown as ProtoGrpcTypeTs;
  const greetServer: GreeterHandlers = {
    SayHello: (
      call: ServerUnaryCall<HelloRequest, HelloReply>,
      callback: sendUnaryData<HelloReply>
    ): void => {
      callback(null, { message: returnValue });
    },
  };

  return [greetServer, proto];
}
async function init(
  returnValue: string,
  pkgName: PkgName
): Promise<[SuperTest<Test>, Server]> {
  const config = await loadConfig();
  let server: Server;
  if (!config.e2e) {
    const [greetServer, proto] = serverSetup(returnValue, pkgName);
    server = new Server();
    // @ts-ignore
    server.addService(proto[pkgName].Greeter.service, greetServer);
    await server.bindAsync(
      config[`${pkgName}Address`],
      ServerCredentials.createInsecure(),
      () => server.start()
    );
  } else {
    server = {
      forceShutdown: () => {},
    } as Server;
  }
  return [supertest(app), server];
}

test.each<{ pkgName: PkgName }>([
  { pkgName: "hellopy" },
  { pkgName: "hellots" },
  { pkgName: "hellors" },
])("test $pkgName routing functionality", async ({ pkgName }) => {
  const name = "TRPC";
  const expected = `Hello ${name} from ${pkgName}`;
  const [client, server] = await init(expected, pkgName);
  const res = await client.get(
    `/trpc/${pkgName}?batch=1&input={"0":"${name}"}`
  );
  assert.deepStrictEqual(res.body[0].result.data, expected);
  server.forceShutdown();
});
