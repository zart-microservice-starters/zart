// Original file: hellors.proto

export interface HelloReply {
  message?: string;
}

export interface HelloReply__Output {
  message: string;
}
