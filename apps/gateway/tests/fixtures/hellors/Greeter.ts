// Original file: hellors.proto

import type * as grpc from "@grpc/grpc-js";
import type { MethodDefinition } from "@grpc/proto-loader";
import type {
  HelloReply as _hellors_HelloReply,
  HelloReply__Output as _hellors_HelloReply__Output,
} from "../hellors/HelloReply";
import type {
  HelloRequest as _hellors_HelloRequest,
  HelloRequest__Output as _hellors_HelloRequest__Output,
} from "../hellors/HelloRequest";

export interface GreeterClient extends grpc.Client {
  SayHello(
    argument: _hellors_HelloRequest,
    metadata: grpc.Metadata,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellors_HelloRequest,
    metadata: grpc.Metadata,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellors_HelloRequest,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellors_HelloRequest,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellors_HelloRequest,
    metadata: grpc.Metadata,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellors_HelloRequest,
    metadata: grpc.Metadata,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellors_HelloRequest,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellors_HelloRequest,
    callback: grpc.requestCallback<_hellors_HelloReply__Output>
  ): grpc.ClientUnaryCall;
}

export interface GreeterHandlers extends grpc.UntypedServiceImplementation {
  SayHello: grpc.handleUnaryCall<
    _hellors_HelloRequest__Output,
    _hellors_HelloReply
  >;
}

export interface GreeterDefinition extends grpc.ServiceDefinition {
  SayHello: MethodDefinition<
    _hellors_HelloRequest,
    _hellors_HelloReply,
    _hellors_HelloRequest__Output,
    _hellors_HelloReply__Output
  >;
}
