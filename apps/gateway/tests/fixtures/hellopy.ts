import type * as grpc from "@grpc/grpc-js";
import type { MessageTypeDefinition } from "@grpc/proto-loader";

import type {
  GreeterClient as _hellopy_GreeterClient,
  GreeterDefinition as _hellopy_GreeterDefinition,
} from "./hellopy/Greeter";

type SubtypeConstructor<
  Constructor extends new (...args: any) => any,
  Subtype
> = {
  new (...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  hellopy: {
    Greeter: SubtypeConstructor<typeof grpc.Client, _hellopy_GreeterClient> & {
      service: _hellopy_GreeterDefinition;
    };
    HelloReply: MessageTypeDefinition;
    HelloRequest: MessageTypeDefinition;
  };
}
