// Original file: hellopy.proto

import type * as grpc from "@grpc/grpc-js";
import type { MethodDefinition } from "@grpc/proto-loader";
import type {
  HelloReply as _hellopy_HelloReply,
  HelloReply__Output as _hellopy_HelloReply__Output,
} from "../hellopy/HelloReply";
import type {
  HelloRequest as _hellopy_HelloRequest,
  HelloRequest__Output as _hellopy_HelloRequest__Output,
} from "../hellopy/HelloRequest";

export interface GreeterClient extends grpc.Client {
  SayHello(
    argument: _hellopy_HelloRequest,
    metadata: grpc.Metadata,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellopy_HelloRequest,
    metadata: grpc.Metadata,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellopy_HelloRequest,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellopy_HelloRequest,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellopy_HelloRequest,
    metadata: grpc.Metadata,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellopy_HelloRequest,
    metadata: grpc.Metadata,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellopy_HelloRequest,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellopy_HelloRequest,
    callback: grpc.requestCallback<_hellopy_HelloReply__Output>
  ): grpc.ClientUnaryCall;
}

export interface GreeterHandlers extends grpc.UntypedServiceImplementation {
  SayHello: grpc.handleUnaryCall<
    _hellopy_HelloRequest__Output,
    _hellopy_HelloReply
  >;
}

export interface GreeterDefinition extends grpc.ServiceDefinition {
  SayHello: MethodDefinition<
    _hellopy_HelloRequest,
    _hellopy_HelloReply,
    _hellopy_HelloRequest__Output,
    _hellopy_HelloReply__Output
  >;
}
