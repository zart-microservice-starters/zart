// Original file: hellopy.proto

export interface HelloRequest {
  name?: string;
}

export interface HelloRequest__Output {
  name: string;
}
