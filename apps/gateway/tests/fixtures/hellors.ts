import type * as grpc from "@grpc/grpc-js";
import type { MessageTypeDefinition } from "@grpc/proto-loader";

import type {
  GreeterClient as _hellors_GreeterClient,
  GreeterDefinition as _hellors_GreeterDefinition,
} from "./hellors/Greeter";

type SubtypeConstructor<
  Constructor extends new (...args: any) => any,
  Subtype
> = {
  new (...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  hellors: {
    Greeter: SubtypeConstructor<typeof grpc.Client, _hellors_GreeterClient> & {
      service: _hellors_GreeterDefinition;
    };
    HelloReply: MessageTypeDefinition;
    HelloRequest: MessageTypeDefinition;
  };
}
